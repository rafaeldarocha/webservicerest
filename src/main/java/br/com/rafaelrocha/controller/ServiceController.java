package br.com.rafaelrocha.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.rafaelrocha.http.User;
import br.com.rafaelrocha.repository.UserRepository;
import br.com.rafaelrocha.repository.entity.UserEntity;

@Path("/service")
public class ServiceController {
		
	private final UserRepository repository = new UserRepository();

	@POST	
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/create")
	public Response create(User user){
		UserEntity entity = new UserEntity();
		try {
			entity.setUsername(user.getUsername());
			entity.setPassword(user.getPassword());
			entity.setName(user.getName());
			entity.setSurname(user.getSurname());
			entity.setEmail(user.getEmail());
			entity.setPhone(user.getPhone());
			entity.setRegisterDate(new Date());
			entity.setEnabled(user.isEnabled());
			repository.save(entity);
            return Response.ok(user, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Create Fail: " + e).type(MediaType.TEXT_PLAIN).build();
		}
	}

	@PUT
	@Produces("application/json; charset=UTF-8")
	@Consumes("application/json; charset=UTF-8")	
	@Path("/update")
	public Response update(User user){
	    if (user != null && user.getId() != null) {
            UserEntity entity = repository.getUser(user.getId());
            try {
                entity.setUsername(user.getUsername());
                entity.setPassword(user.getPassword());
                entity.setName(user.getName());
                entity.setSurname(user.getSurname());
                entity.setEmail(user.getEmail());
                entity.setPhone(user.getPhone());
                entity.setEnabled(user.isEnabled());

                repository.update(entity);
                return Response.ok(user, MediaType.APPLICATION_JSON).build();
            } catch (Exception e) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Update Fail: " + e).type(MediaType.TEXT_PLAIN).build();
            }
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Update Fail: Id field is required for this action!").type(MediaType.TEXT_PLAIN).build();
        }
	}

	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/getAllUsers")
	public List<User> getAllUsers(){
		List<User> users = new ArrayList<>();
		List<UserEntity> userEntityList = repository.getAllUser();
		userEntityList.forEach(entity -> users.add(new User(entity.getId(), entity.getUsername(), entity.getPassword(), entity.getName(), entity.getSurname(), entity.getEmail(), entity.getPhone(), entity.getRegisterDate(), entity.isEnabled())));
		return users;
	}

	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/findUser/{filterValue}")
	public List<User> findUser(@PathParam("filterValue") String strFilter){
		List<User> users = new ArrayList<>();
		List<UserEntity> userEntityList = repository.findUser(strFilter);
        userEntityList.forEach(entity -> users.add(new User(entity.getId(), entity.getUsername(), entity.getPassword(), entity.getName(), entity.getSurname(), entity.getEmail(), entity.getPhone(), entity.getRegisterDate(), entity.isEnabled())));
		return users;
	}

	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/getUser/{id}")
	public Response getUser(@PathParam("id") Integer id){
		UserEntity entity = repository.getUser(id);
		if (entity != null) {
            User usr =  new User(entity.getId(), entity.getUsername(), entity.getPassword(), entity.getName(), entity.getSurname(), entity.getEmail(), entity.getPhone(), entity.getRegisterDate(), entity.isEnabled());
			return Response.ok(usr, MediaType.APPLICATION_JSON).build();
        }
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("User not found!").type(MediaType.TEXT_PLAIN).build();
	}

	@DELETE
	@Produces("application/json; charset=UTF-8")
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") Integer id){
		try {
            UserEntity entity = repository.getUser(id);
            if (entity == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Delete Fail: User not found!").type(MediaType.TEXT_PLAIN).build();
            }
            repository.delete(entity);
			return Response.ok(entity, MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Delete Fail: " + e.getMessage()).type(MediaType.TEXT_PLAIN).build();
		}
	}
	
}

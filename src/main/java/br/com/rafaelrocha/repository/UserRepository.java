package br.com.rafaelrocha.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.rafaelrocha.repository.entity.UserEntity;

public class UserRepository {

	private final EntityManagerFactory entityManagerFactory;
	
	private final EntityManager entityManager;
	
	public UserRepository(){
		this.entityManagerFactory = Persistence.createEntityManagerFactory("PersistenceUnitZallpyTest");
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	public void save(UserEntity userEntity){
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(userEntity);
		this.entityManager.getTransaction().commit();
	}

	public void update(UserEntity userEntity){
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(userEntity);
		this.entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<UserEntity> findUser(String filter) {
		List<UserEntity> userList = this.entityManager.createQuery("SELECT p FROM UserEntity p WHERE UPPER(p.username) LIKE '%"+filter.toUpperCase()+"%' ORDER BY p.name").getResultList();
		if (userList.isEmpty()) {
			userList = this.entityManager.createQuery("SELECT p FROM UserEntity p WHERE UPPER(p.name) LIKE '%"+filter.toUpperCase()+"%' ORDER BY p.name").getResultList();
		}
		if (userList.isEmpty()) {
			userList = this.entityManager.createQuery("SELECT p FROM UserEntity p WHERE UPPER(p.email) LIKE '%"+filter.toUpperCase()+"%' ORDER BY p.name").getResultList();
		}
		return userList;
	}

	public List<UserEntity> getAllUser(){
		return this.entityManager.createQuery("SELECT p FROM UserEntity p ORDER BY p.name").getResultList();
	}

	public UserEntity getUser(Integer id){
		return this.entityManager.find(UserEntity.class, id);
	}

	public void delete(UserEntity usr){
		this.entityManager.getTransaction().begin();
		this.entityManager.remove(usr);
		this.entityManager.getTransaction().commit();
		
	}
}

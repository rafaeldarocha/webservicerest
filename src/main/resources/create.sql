CREATE TABLE TB_USER
(
    id SERIAL PRIMARY KEY,
    username VARCHAR(60) NOT NULL,
    password VARCHAR(40) NOT NULL,
    is_enabled BOOLEAN NOT NULL,
    register_date TIMESTAMP NOT NULL,
    name VARCHAR(60) NOT NULL,
    surname VARCHAR(60) NOT NULL,
    email VARCHAR(80) NOT NULL,
    phone VARCHAR(20) NOT NULL
);
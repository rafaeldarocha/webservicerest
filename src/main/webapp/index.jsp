<!DOCTYPE html>
<%@ page isELIgnored="false" %>
<html>
<head>
<title>Zallpy - User Test</title>
</head>
<body>
	<div style="padding-left:50px;font-family:monospace;">CRUD Operations</br></br>
		<a href="${pageContext.request.contextPath}/create.jsp"><div style="color:saffron">Create User</div></a></br></br>
		<a href="${pageContext.request.contextPath}/list.jsp"><div style="color:saffron">List Users</div></a></br></br>
		<a href="${pageContext.request.contextPath}/delete.jsp"><div style="color:saffron">Delete User</div></a></br></br>
	</div>
</body>
</html>
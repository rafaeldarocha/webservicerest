<!DOCTYPE html>
<html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="js/rest.js"></script>
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <title>Zallpy - Delete User</title>
    <script src="js/rest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        prepareSubmit("rest/service/delete/{id}", "delete", "deleted");
    </script>
</head>
<body>
<div style="padding-left:50px;font-family:monospace;">
    <h2>Delete User</h2>
    <form id="userForm">
        <div style="width: 200px; text-align:left;">
            <div style="padding:15px;">
                User ID: <input name="id"/>
            </div>
            <br/>
            <span style="padding:10px;">
                <input type="button" onclick="location.href='.';" value="Back"/>
                <input id="inputSubmit" type="submit" value="Delete" onclick="return false;"/>
            </span>
        </div>
    </form>
</div>
</body>
</html>
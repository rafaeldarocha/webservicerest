String.prototype.contains = function (value) {
    return this.indexOf(value) > -1;
};

function getFormDataObject(htmlForm) {
    var unindexed_array = htmlForm.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function(n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

function getFormData(htmlForm) {
    return JSON.stringify(getFormDataObject(htmlForm));
}

function updateURL(htmlForm, url) {
    var indexed_array = getFormDataObject(htmlForm);
    if (url.contains("{") && url.contains("}")) {
        for (attr in indexed_array) {
            if (url.contains("{"+attr+"}")) {
                url = url.replace("{"+attr+"}", indexed_array[attr]);
            }
        }
    }
    return url;
}

function prepareSubmit(url, methodType, actionType) {
    $(function() {
        $('#inputSubmit').click(function(e) {
            $.ajax({
                url: updateURL($('#userForm'), url),
                type: methodType,
                dataType: 'json',
                contentType: 'application/json',
                data : getFormData($('#userForm'), url),
                success : function(data) {
                    alert("User "+data.username+" successfully "+actionType+"!");
                    window.location.reload();
                },
                error: function (err) {
                    alert(err.status+" - "+err.statusText+": "+err.responseText);
                }
            });
        });
    });
}

function getUserObject(id, fnCallback) {
    $(function() {
        $.ajax({
            url: "rest/service/getUser/"+id,
            type: "get",
            dataType: 'json',
            contentType: 'application/json',
            success : function(data) {
                fnCallback.call(this, data);
            },
            error: function (err) {
                alert(err.status+" - "+err.statusText+": "+err.responseText);
            }
        });
    });
}

function deleteUser(id) {
    $(function() {
        $.ajax({
            url: "rest/service/delete/"+id,
            type: "delete",
            dataType: 'json',
            contentType: 'application/json',
            success : function(data) {
                alert("User "+data.username+" successfully deleted!");
                window.location.reload();
            },
            error: function (err) {
                alert(err.status+" - "+err.statusText+": "+err.responseText);
            }
        });
    });
}
<!DOCTYPE html>
<html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="js/rest.js"></script>
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <title>Zallpy - User List</title>
    <script src="js/rest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $(document).ready(function () {
                findUser();
            });
        });
        function findUser() {
            $(function () {
                $.ajax({
                    url: "rest/service/findUser/"+$("#search")[0].value+"%",
                    type: "get",
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (list) {
                        drawItems(list)
                    },
                    error: function (err) {
                        alert(err.status + " - " + err.statusText + ": " + err.responseText);
                    }
                });
            });
            return false;
        }

        function drawItems(list) {
            $('#divResult')[0].innerHTML = "";
            var table = $('<table border="1" style="border-collapse: collapse;"/>').appendTo($('#divResult'));
            $(list).each(function (i, user) {
                if (i === 0) {
                    var trTitle = $('<tr/>').appendTo(table);
                    for (attr in user) {
                        trTitle.append($('<td style="padding: 5px; font-weight: bold;"/>').text(attr.toUpperCase()));
                    }
                    trTitle.append($('<td colspan="2" style="text-align: center; padding: 5px; font-weight: bold;"/>').text("ACTIONS"));
                }
                var tr = $('<tr/>').appendTo(table);
                for (attr in user) {
                    tr.append($('<td style="padding: 5px;"/>').text(user[attr]));
                }
                tr.append($('<td/>').append('<input type="button" onclick="location.href=\'update.jsp?userId=' + user.id + '\'" value="Edit" />'));
                tr.append($('<td/>').append('<input type="button" onclick="deleteUser(' + user.id + ')" value="Delete" />'));
            });
        }
    </script>
</head>
<body>
<div style="padding-left:50px;font-family:monospace;">
    <h2>User List</h2>
    <form method="post" onsubmit="return findUser();">
        <div style="width: 500px; text-align:left; padding:10px;">
            Search: <input type="text" id="search">
        </div>
    </form>
    <form id="userForm">
        <div style="width: 100%; text-align:left; padding:10px;">
            <div id="divResult"></div>
            <br/>
            <span>
                <input type="button" onclick="location.href='.';" value="Back"/>
            </span>
        </div>
    </form>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Zallpy - User Update</title>

    <style type="text/css">
        span.error{
            color: red;
            margin-left: 5px;
        }
    </style>

    <script src="js/rest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        var fnCallBack = function (userObj) {
            for(var prop in userObj) {
                var inputField = $("#userForm input[name="+prop+"]")[0];
                if (inputField != null) {
                    if (inputField.type === "checkbox") {
                        inputField.checked = userObj[prop];
                    } else {
                        inputField.value = userObj[prop];
                    }
                }
            }
        };
        getUserObject(${param.userId}, fnCallBack);
        prepareSubmit("rest/service/update", "put", "updated");
    </script>
</head>
<body>
<h1>Update User</h1>
<hr />

<form method="post" id="userForm">
    <div style="width: 100px; text-align:left;">
        <div style="padding:10px;">
            User Id: <input name="id" readonly="true" style="background: #dddddd;"/>
        </div>
        <div style="padding:10px;">
            Username: <input name="username" />
        </div>
        <div style="padding:10px;">
            password: <input name="password" />
        </div>
        <div style="padding:10px;">
            Name: <input name="name" />
        </div>
        <div style="padding:10px;">
            Surname: <input name="surname" />
        </div>
        <div style="padding:10px;">
            E-mail: <input name="email" />
        </div>
        <div style="padding:10px;">
            Phone: <input name="phone" />
        </div>
        <div style="padding:10px;">
            Enabled: <input type="checkbox" name="isEnabled" value="true"/>
        </div>
        <br/>
        <span style="padding:10px;">
            <input type="button" onclick="location.href='.';" value="Back" />
            <input id="inputSubmit" type="submit" value="Save" onclick="return false;" />
        </span>
    </div>
</form>

</body>
</html>
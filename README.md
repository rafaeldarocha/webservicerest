# Exemplo de utilização de Webservice REST com JavaEE

- Utilizado Java 8 (jdk1.8.0_181)
- Necessário ter o Maven instalado
- Necessário ter um server de banco de dados instalado (no exemplo foi utilizado o PostgreSQL)
- Acessar o DB e rodar o script contido no aruqivo: WebServiceRest/src/main/resources/create.sql
- Na pasta do projeto através do "terminal" digitar o seguinte comando: mvn clean install
- Pegar o war gerado na pasta target e implantar em algum servidor de aplicação: Jboss, WebLogic, Tomcat, etc.
- Abrir a aplicação em algum navegador: http://localhost:7001/WebServiceRest/ (7001 é a porta utilizada pelo weblogic, outros servidores utilizam portas diferentes)